<?php
 
class AccountController extends Controller
{
	public $layout='//layouts/column1';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * Displays the login page
	 */
	public function actionIndex()
	{
		$model=new accountForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='account-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['AccountForm']))
		{
			$model->attributes=$_POST['AccountForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->save())
			{
				$criteria=new CDbCriteria;
				$criteria->compare('t.user_id',Yii::app()->user->id);
				
				$dataProvider=new CActiveDataProvider('Subscribe',
														array(
															'criteria'=>$criteria,
													));
				$this->redirect(Yii::app()->createUrl('/home/'),array(
					'dataProvider'=>$dataProvider,
				));
			}
		}
		// display the login form
		$this->render('index',array('model'=>$model));
	}
}