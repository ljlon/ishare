<?php

class SubscribeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update','delete','swap'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($feed_id)
	{
		$model=Subscribe::model()->findByAttributes(array('feed_id'=>$feed_id, 'user_id'=>Yii::app()->user->id));

		if($model==null)
		{
			$model=new Subscribe;
			$model->feed_id = $feed_id;
			$model->user_id = Yii::app()->user->id;
			if($model->save())
			{
				
			}
			else
			{
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
			}
		}
		else
		{
			if ($model->user_id == Yii::app()->user->id)
			{
				$this->loadModel($model->id)->delete();
			}
			else
			{
				throw new CHttpException(403,'Access denied.');
			}
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			{
				//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionSwap()
	{
		if(isset($_POST['srcSubID']) && isset($_POST['dstSubID']))
		{
			$srcSubID = $_POST['srcSubID'];
			$dstSubID = $_POST['dstSubID'];
			$src=SubscribeOrder::model()->findByAttributes(array('subscribe_id'=>$srcSubID));
			$src->delete();
			$dst=SubscribeOrder::model()->findByAttributes(array('subscribe_id'=>$dstSubID));
			$dst->delete();
			
			$newSrc = new SubscribeOrder;
			$newSrc->id = $src->id;
			$newSrc->subscribe_id = $dst->subscribe_id;
			
			$newDst = new SubscribeOrder;
			$newDst->id = $dst->id;
			$newDst->subscribe_id = $src->subscribe_id;
			
			$newSrc->save();
			$newDst->save();
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Subscribe::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Subscribe-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
