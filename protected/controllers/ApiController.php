<?php

class ApiController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'explore' and 'view' actions
				'actions'=>array('feedView', 'feedCategory'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' 'index' and 'update' actions
				'actions'=>array('feedCreate'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionFeedView($id)
	{
		$model=Feed::model()->findByPk($id);
		if($model===null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}
		else
		{
			$this->render('view',array(
										'model'=>$model,
									));
		}
	}

	/**
	 * Creates a new feed model.
	 */
	public function actionFeedCreate()
	{
		$model=new Feed;

		if(isset($_POST['Feed']))
		{
			$model->attributes=$_POST['Feed'];
			$model->user_id=Yii::app()->user->id;
			if($model->save())
			{
				$this->redirect(array('feedView','id'=>$model->id));
			}
		}
		else
		{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * This is the action to 'feedCategory'.
	 */
	public function actionFeedCategory()
	{
		$feedCategory= new FeedCategory;
		$model = $feedCategory->load();
		$modelArray = array();
		
		foreach ($model->data as $key => $value)
		{
			$modelArray[$key] = array('id'=>urlencode($value->id), 'name'=>urlencode($value->name));
		}
		echo urldecode ( json_encode ( $modelArray ) );  
	}
}
