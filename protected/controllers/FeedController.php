<?php

class FeedController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	public $sortMenu = null;
	public $category = null;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'explore' and 'view' actions
				'actions'=>array('view', 'index', 'random'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' 'index' and 'update' actions
				'actions'=>array('create','update', 'home'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Feed;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Feed']))
		{
			$model->attributes=$_POST['Feed'];
			$model->user_id=Yii::app()->user->id;
			if($model->save())
				$this->redirect(array('home'));
		}

		$feedCategory = new FeedCategory;
		$categoryProvider=$feedCategory->load();
		
		$this->render('create',array(
			'model'=>$model,
			'dataProvider'=>$categoryProvider,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Feed']))
		{
			$model->attributes=$_POST['Feed'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$feedCategory = new FeedCategory;
		$categoryProvider=$feedCategory->load();
		
		$this->render('update',array(
			'model'=>$model,
			'dataProvider'=>$categoryProvider,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists user's models.
	 */
	public function actionHome($cat = null)
	{
		$feed = new Feed;
		$dataProvider = $feed->load(Yii::app()->user->id, $cat);
		
		$feedCategory = new FeedCategory;
		$categoryProvider=$feedCategory->load();
			
		$this->render('home',array(
			'dataProvider'=>$dataProvider,
			'categoryProvider'=>$categoryProvider,
			'categoryID'=>$cat,
		));
	}
	
	/**
	 * This is the action to 'index'.
	 */
	public function actionIndex($by = null, $cat = null)
	{
		$feed = new Feed;
		if ($by == 0)
			$dataProvider = $feed->load(null, $cat, 'subCount');
		else if ($by == 1)
			$dataProvider = $feed->load(null, $cat, 'id' );
		else if ($by == 2)
			$dataProvider = $feed->load(null, $cat, 'random');
		else
			$dataProvider = $feed->load(null, $cat, 'subCount');
		
		$feedCategory = new FeedCategory;
		$categoryProvider=$feedCategory->load();

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'categoryProvider'=>$categoryProvider,
			'sortID'=>$by,
			'categoryID'=>$cat,
		));
	}
	
	/**
	 * Manages all models.
	 */
	 /*
	public function actionAdmin()
	{
		$model=new Feed('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Feed']))
			$model->attributes=$_GET['Feed'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Feed::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='feed-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
