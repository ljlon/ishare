<?php

/**
 * This is the model class for table "tbl_feed".
 *
 * The followings are the available columns in table 'tbl_feed':
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property string $img_link
 * @property string $description
 * @property integer $user_id
 */
class Feed extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_feed';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, link, original_img_link, thumb_img_link, mid_img_link, user_id, category_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('name, link, original_img_link, thumb_img_link, mid_img_link', 'length', 'max'=>100),
			array('description', 'length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, original_img_link, thumb_img_link, mid_img_link, description, user_id, user_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
			'category'=>array(self::BELONGS_TO, 'FeedCategory', 'category_id'),
			'subscribe'=>array(self::HAS_MANY, 'Subscribe', 'feed_id'),
			'subCount' => array(self::STAT, 'Subscribe', 'feed_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => '名称',
			'link' => '地址',
			'thumb_img_link' => '缩略图图片',
			'mid_img_link' => '中等尺寸图片',
			'original_img_link' => '原尺寸图片',
			'description' => '描述',
			'user_id' => '用户',
			'category_id' => '目录',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.link',$this->link,true);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.user_id',$this->user_id);
		$criteria->compare('t.category_id',$this->category_id);
		
		//$criteria->compare('subscribe.user_id',1);
		
		if (isset(Yii::app()->user->id))
		{
			$userCondition = 'subscribe.user_id=';
			$userCondition .= Yii::app()->user->id;
			$criteria->with=array(
					'user',
					'category',
					'subscribe'=>array('condition'=>$userCondition),
				);
		}
		else
		{
			$criteria->with=array(
					'user',
					'category',
					);
		}
		
		//$Criteria->join = 'LEFT JOIN tpl_subscribe ON tpl_subscribe.pin_id = t.id';
		return new CActiveDataProvider( $this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'user_name'=>array(
						'asc'=>'user.username',
						'desc'=>'user.username DESC',
					),
					'*',
				),
			),
			'pagination'=>array(
				'pageSize'=>20,
			),
		));
	}

	public static function GetPinWithSubStatus($sub_user_id)
	{
		$criteria = new CDbCriteria();
	 
		$pins = Pin::model()->findAll($criteria);

		foreach($pins as $pin)
		{
			Pin::model()->getSubStatus($sub_user_id, $pin->id);
			$pin->is_subed_by_user = true;
			$pin->$subscribe_id = 0;
		}
		
		return $pins;
	}
	
	public function load($userID = NULL, $catID = null, $orderBy = null)
	{
		$criteria=new CDbCriteria;
		$sub_table = Subscribe::model()->tableName();
		$sub_count_sql = "(select count(*) from $sub_table pt where pt.feed_id = t.id)";
 
		// select
		$criteria->with=array(
					'subCount',
					);
	
		if ($catID != null)
		{
			$criteria->compare('t.category_id',$catID);
		}
		if ($userID != null)
		{
			$criteria->compare('t.user_id',Yii::app()->user->id);
		}
		
		if ($orderBy == 'id')
		{
			$criteria->order="t.id DESC";
		}
		else if ($orderBy == 'random')
		{
			$criteria->order="RAND()";
		}
		else if ($orderBy == 'subCount')
		{
			$criteria->order=$sub_count_sql . "DESC";
		}
		
		return new CActiveDataProvider('Feed',array(
			'criteria'=>$criteria,
		));
	}
}

	