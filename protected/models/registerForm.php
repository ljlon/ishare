<?php

/**
 * RegisterForm class.
 * RegisterForm is the data structure for keeping
 * user register form data. It is used by the 'register' action of 'SiteController'.
 */
class RegisterForm extends CFormModel
{
	public $username;
	public $email;
	public $password;
	public $passwordAgain;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, email, password, passwordAgain', 'required', 'message'=>'请输入{attribute}'),
			array('email', 'length', 'max'=>128),
			array('username', 'length', 'min'=>4, 'max'=>24, 
				'tooShort'=>Yii::t("translation", "{attribute}长度不能少于{min}个字符."),
				'tooLong'=>Yii::t("translation", "{attribute}长度不能大于{max}个字符.")),
			array('password, passwordAgain', 'length', 'min'=>6, 'max'=>24,
					'tooShort'=>Yii::t("translation", "密码长度不能少于{min}个字符."),
					'tooLong'=>Yii::t("translation", "密码长度不能大于{max}个字符.")),
			// password needs to be authenticated
			array('username, password, passwordAgain', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'username'=>'用户名',
			'email'=>'邮箱',
			'password'=>'密码',
			'passwordAgain'=>'确认密码'
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$record=User::model()->findByAttributes(array('username'=>$this->username));
			if($record!=null)
			{
				$this->addError('username','用户名已存在');
			}
				
			if ($this->password != $this->passwordAgain)
			{
				$this->addError('password','两次输入的密码不一致');
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function register()
	{
		$user = new User;
		$user->username = $this->username;
		$user->email = $this->email;
		$user->password = hash('sha256', $this->password); 
		$record=User::model()->findByAttributes(array('username'=>$this->username));
		if($record!=null)
		{
			return false;
		}
		
		if($user->save())
		{
			return true;
		}
		
		return false;
	}
}
