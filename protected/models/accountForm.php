<?php

/**
 * RegisterForm class.
 * RegisterForm is the data structure for keeping
 * user register form data. It is used by the 'register' action of 'SiteController'.
 */
class AccountForm extends CFormModel
{
	public $oldPassword;
	public $newPassword;
	public $passwordAgain;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('oldPassword, newPassword, passwordAgain', 'required', 'message'=>'请输入{attribute}'),
			array('oldPassword, newPassword, passwordAgain', 'length', 'min'=>6, 'max'=>24,
					'tooShort'=>Yii::t("translation", "密码长度不能少于{min}个字符."),
					'tooLong'=>Yii::t("translation", "密码长度不能大于{max}个字符.")),
			
			// password needs to be authenticated
			array('oldPassword, newPassword, passwordAgain', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'oldPassword'=>'当前密码',
			'newPassword'=>'新密码',
			'passwordAgain'=>'确认密码'
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			if($this->_identity===null)
			{
				$this->_identity=new UserIdentity(Yii::app()->user->name,$this->oldPassword);
				$this->_identity->authenticate();
			}
			if($this->_identity->errorCode!=UserIdentity::ERROR_NONE)
			{
				$this->addError('oldPassword','密码错误');
			}
				
			if ($this->newPassword != $this->passwordAgain)
			{
				$this->addError('newPassword','两次输入的密码不一致');
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function save()
	{
		$record=User::model()->findByAttributes(array('username'=>Yii::app()->user->name));
		if($record==null)
		{
			return false;
		}
		
		$record->password = hash('sha256', $this->newPassword); 
		if($record->save())
		{
			return true;
		}
		
		return false;
	}
}
