<?php

/**
 * This is the model class for table "tbl_subscribe".
 *
 * The followings are the available columns in table 'tbl_subscribe':
 * @property integer $id
 * @property integer $feed_id
 * @property integer $user_id
 */
class Subscribe extends CActiveRecord
{
	public $user_id;
	
	public $user_name;
	
	public $feed_id;
	
	public $feed_name;
	
	public $feed_link;
	
	public $feed_img_link;
	
	public $feed_description;
	
	public $feed_user_id;
	
	public $feed_user_name;
			
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Subscribe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function save($runValidation = true, $attributes = NULL)
	{
		$ret = parent::save($runValidation, $attributes);
		if ($ret)
		{
			$subscribeOrder = new SubscribeOrder();
			$subscribeOrder->subscribe_id = $this->id;
			$ret = $subscribeOrder->save();
		}
		return $ret;
	}
	
	public function delete()
	{
		$model=SubscribeOrder::model()->findByAttributes(array('subscribe_id'=>$this->id));
		if ($model != null)
		{
			$model->delete();
		}
		
		return parent::delete();
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_subscribe';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('feed_id, user_id', 'required'),
			array('feed_id, user_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, user_name, feed_id, feed_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'feed'=>array(self::BELONGS_TO, 'Feed', 'feed_id'),
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => '用户ID',
			'user_name' => '用户名',
			'feed_id' => 'feed ID',
			'feed_name' => '名称',
			'feed_link' => '地址',
			'feed_img_link' => '图片地址',
			'feed_description' => '描述',
			'feed_user_id' => 'feed用户ID',
			'feed_user_name' => 'feed用户名',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.feed_id',$this->feed_id);
		$criteria->compare('t.user_id',$this->user_id);

		$criteria->compare('feed.name',$this->feed_name, true);

		$criteria->compare('user.username', $this->user_name, true);
		
		$criteria->with = 'feed';
		$criteria->with = 'user';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}