<?php

/**
 * This is the model class for table "tbl_subscribe_order".
 *
 * The followings are the available columns in table 'tbl_subscribe_order':
 * @property integer $id
 * @property integer $subscribe_id
 */
class SubscribeOrder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Subscribe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_subscribe_order';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subscribe_id', 'required'),
			array('subscribe_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, subscribe_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subscribe'=>array(self::BELONGS_TO, 'Subscribe', 'subscribe_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subscribe_id' => '订阅ID',
		);
	}
	
	public function load()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('subscribe.user_id',Yii::app()->user->id);
		$criteria->with = 'subscribe';
		$criteria->order='t.id DESC';
		
		return new CActiveDataProvider('SubscribeOrder',
												array(
													'criteria'=>$criteria,
													'pagination'=>array(
														'pageSize'=>18,
														),
											));
	}
}