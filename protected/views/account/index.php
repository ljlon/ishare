<?php
$this->pageTitle=Yii::app()->name . ' - 登陆';
?>

<div id="account" >
	<h4 class="account-head">修改密码</h4>
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'type'=>'horizontal',
		'id'=>'account-form',
		'enableAjaxValidation'=>true,
	)); ?>

	<?php echo $form->passwordFieldRow($model,'oldPassword', array('class'=>'span2')); ?>

	<?php echo $form->passwordFieldRow($model,'newPassword', array('class'=>'span2')); ?>

	<?php echo $form->passwordFieldRow($model,'passwordAgain', array('class'=>'span2')); ?>

	<div class="control-group">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'保存',
			'htmlOptions'=>array(
				'class'=>'span2 controls',
				),
		)); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>
