<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
	
</head>

<script type="text/javascript">
        $(document).ready(function () {
            $("ul[id*=searchDropdown] li").click(function () {
				var searchForm=document.getElementById("searchForm");
				var searchInput=document.getElementById("searchInput");
				if ($(this).text() == " 百度")
				{
					searchForm.attributes["search_link"].nodeValue = "http://www.baidu.com/baidu";
					searchInput.name = "word";
					searchInput.placeholder = "百度搜索";
				}
				else if ($(this).text() == " 谷歌")
				{
					searchForm.attributes["search_link"].nodeValue = "https://www.google.com.hk/search";
					searchInput.name = "q";
					searchInput.placeholder = "谷歌搜索";
				}
				else if ($(this).text() == " 淘宝")
				{
					searchForm.attributes["search_link"].nodeValue = "http://s.taobao.com/search";
					searchInput.name = "q";
					searchInput.placeholder = "淘宝搜索";
				}
            });
			$("ul[id*=searchDropdown] li").mouseover(function () {
				$(this).addClass('active'); 
			});
			$("ul[id*=searchDropdown] li").mouseout(function () {
				$(this).removeClass('active'); 
			});
			$("form[id*=searchForm]").submit(function () {
				var searchInput=document.getElementById("searchInput");
				searchInput.value = "";
			});
        });
		
		function searchAction()
		{
			var searchForm=document.getElementById("searchForm");
			var searchInput=document.getElementById("searchInput");
			
			var url = searchForm.attributes["search_link"].nodeValue + '?' + searchInput.name + '=' + searchInput.value;
			window.open(url, '_blank');
			
			return false;
		}
</script>

<body>
<?php 
	$search = 	'<form id="searchForm" class="input-prepend navbar-search" method="get" onsubmit="return searchAction();" search_link="https://www.google.com.hk/search" target="_blank">
					<div class="btn-group">
						<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
							<span class="caret"></span>
						</button>
						<ul id="searchDropdown" class="dropdown-menu" style="cursor:pointer" >
						  <li name="google" value="https://www.google.com.hk/search"> 谷歌</li>
						  <li name="baidu" value="https://www.baidu.com/search"> 百度</li>
						  <li name="baidu" value="https://www.baidu.com/search"> 淘宝</li>
						</ul>
					</div>
					<input class="span3" id="searchInput" type="text" name="q" placeholder="谷歌搜索" />
					<button class="btn" type="submit" style="display:none">Go!</button>
				</form>';
	
	$this->widget('bootstrap.widgets.TbNavbar',array(
	//'type'=>'inverse',
	'brand'=>'首页',
	'brandUrl'=>array('/Home/'),
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
				array('label'=>'发现', 'url'=>array('/Feed/')),
            ),
        ),
		$search,
		array(
            'class'=>'bootstrap.widgets.TbMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array('label'=>'注册', 'url'=>array('/site/register'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'登陆', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>''.Yii::app()->user->name.'', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'我的分享', 'url'=>array('/feed/home')),
					array('label'=>'修改密码', 'url'=>array('/account/')),
                    array('label'=>'退出', 'url'=>array('/site/logout')),
                )),
            ),
		),
    ),
)); ?>

<div class="container" id="page">
	<?php /*$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			 'homeLink'=>CHtml::link('首页',Yii::app()->createUrl('home/')), 
			 'links'=>$this->breadcrumbs,
			));*/ ?>
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by 小丑尼莫.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
