<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php
	$this->widget('bootstrap.widgets.TbMenu', array(
		'type'=>'tabs',
		'stacked'=>false,
		'items'=>$this->menu,
		'htmlOptions'=>array('class'=>'operations'),
	));
?>
		
<div class="row">
	<div id="sidebar" class="span2">
		
		<?php	
			if (isset($this->sortMenu)):
				$this->beginWidget('zii.widgets.CPortlet', array(
					'title'=>'浏览',
				));
				$this->widget('bootstrap.widgets.TbMenu', array(
					'type'=>'tabs',
					'stacked'=>true,
					'items'=>$this->sortMenu,
					//'htmlOptions'=>array('class'=>'category'),
				));
				$this->endWidget();
			endif;
			if (isset($this->category)):
				$this->beginWidget('zii.widgets.CPortlet', array(
					'title'=>'类别',
				));
				$this->widget('bootstrap.widgets.TbMenu', array(
					'type'=>'tabs',
					'stacked'=>true,
					'items'=>$this->category,
					//'htmlOptions'=>array('class'=>'category'),
				));
				$this->endWidget();
			endif;
		?>
	</div><!-- sidebar -->
	<div id="content" class="span10">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>