<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div>
<?php
	$this->widget('bootstrap.widgets.TbMenu', array(
		'type'=>'tabs',
		'stacked'=>false,
		'items'=>$this->menu,
		'htmlOptions'=>array('class'=>'operations'),
	));
?>
</div>

<div id="content">
	<?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>