<?php

if( Yii::app()->user->isGuest )
{
	//$url = Yii::app()->createUrl('feed/index');  
	//echo "<script type='text/javascript'>";  
	//echo "window.location.href='$url'";  
	//echo "</script>";  
	//exit;
}

$this->pageTitle=Yii::app()->name;
?>

<script language="javascript">
function mouseOver(unSubButton, feedButton)
{
	if (bDragging == false)
	{
		if (!unSubButton)
		{
		}
		else
		{
			unSubButton.style.display='';
		}
		if (!feedButton)
		{
		}
		else
		{
			feedButton.style.display='';
		}
	}
}

function mouseOut(unSubButton, feedButton)
{
	if (bDragging == false)
	{
		if (!unSubButton)
		{
		}
		else
		{
			unSubButton.style.display='none';
		}
		if (!feedButton)
		{
		}
		else
		{
			feedButton.style.display='none';
		}
	}
}

bDragging = false;
ajaxSwapUrl = '<?php echo Yii::app()->createUrl("subscribe/swap") ?>';
dragParent = null;
dragOptions = {
	revert:true,
	/*axis: 'x',*/
	opacity: 0.8,
	start: function(event, ui) {
		dragParent = $(this).parent();
		bDragging = true;
	},
	stop: function(event, ui) {
		bDragging = false;
	}
};

</script> 

<?php if( !Yii::app()->user->isGuest ): ?>
	<?php 
		if ($dataProvider->itemCount != 0): 
			$this->widget('bootstrap.widgets.TbThumbnails', array(
				'id'=>'sub-thumb',
				'dataProvider'=>$dataProvider,
				'template'=>"{items}\n{pager}",
				'itemView'=>'/subscribe/_view',
				'afterAjaxUpdate'=>'js:function(){
											bDragging = false;
										}',
				'enablePagination'=>false,
			)); 
		else: ?>
			<?php 
			$this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
			'heading'=>'您好，朋友',
			'headingOptions'=>array(
					'id'=>'heroHeading',
				),
			)); ?>
			<p>还没有属于自己的炫酷主页吗？赶快开始定制吧！</p>
				<p><?php $this->widget('bootstrap.widgets.TbButton', array(
				'type'=>'primary',
				'size'=>'large',
				'label'=>'开始定制',
				'url'=>array('/feed/')
			)); ?></p>
			<?php $this->endWidget(); ?>
	<?php endif; ?>
<?php else: ?>
	<?php 
		$this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
		'heading'=>'您好，朋友',
		'headingOptions'=>array(
				'id'=>'heroHeading',
			),
	)); ?>
	<p> </p>
	<p>赶快加入我们，定制属于自己的炫酷主页吧！</p>
	<p><?php $this->widget('bootstrap.widgets.TbButton', array(
		'type'=>'primary',
		'size'=>'large',
		'label'=>'加入我们',
		'url'=>array('/site/register')
	)); ?></p>
	 
	<?php $this->endWidget(); ?>
<?php endif; ?>

