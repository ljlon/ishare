<?php
$this->menu=array(
	array('label'=>'我的分享','url'=>array('home'),'active'=>true),
	array('label'=>'分享酷站','url'=>array('create')),
);
?>

<?php
$links = null;
foreach ($categoryProvider->getData() as $key=>$value)
{
	$this->category[$value->id] = array('label'=>$value->name,'url'=>Yii::app()->createUrl('feed/home', array('cat'=>$value->id)));
}
if (isset($categoryID))
{
	$this->category[$categoryID]['active']=true;
	$links = array($this->category[$categoryID]['label']);
}
?>

<?php 
	$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
		'homeLink'=>CHtml::link('全部', Yii::app()->createUrl('feed/home')), 
		'links'=>$links,
)); ?>

<?php 	if ($dataProvider->itemCount != 0): 
			$this->widget('bootstrap.widgets.TbThumbnails', array(
				'id'=>'feed-thumb',
				'dataProvider'=>$dataProvider,
				'template'=>"{items}\n{pager}",
				'itemView'=>'_view',
			)); 
		endif;
?>