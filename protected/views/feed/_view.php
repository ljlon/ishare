<script language="javascript">
function showUnSub(unSubButton)
{
	if (unSubButton.textContent == '已订阅')
	{
		unSubButton.className ='btn btn-danger btn-mini';
		unSubButton.textContent = '取消订阅';
	}
}

function hideUnSub(unSubButton)
{
	if (unSubButton.textContent == '取消订阅')
	{
		unSubButton.className ='btn btn-success btn-mini';
		unSubButton.textContent = '已订阅';
	}
}

function mouseOver(unSubButton, modifyButton)
{
	if (!unSubButton)
	{
	}
	else
	{
		unSubButton.style.display='';
	}
	if (!modifyButton)
	{
	}
	else
	{
		modifyButton.style.display='';
	}
}

function mouseOut(unSubButton, modifyButton)
{
	if (!unSubButton)
	{
	}
	else
	{
		unSubButton.style.display='none';
	}
	if (!modifyButton)
	{
	}
	else
	{
		modifyButton.style.display='none';
	}
}
</script> 

<?php
	$unSubButtonID = 'unSubButton';
	$unSubButtonID .= $data->id;
	$modifyButtonID = 'modifyButton';
	$modifyButtonID .= $data->id;
	$detailButtonID = 'detailButton';
	$detailButtonID .= $data->id;
		
	$showUnSubProc = 'showUnSub(';
	$showUnSubProc .= $unSubButtonID;
	$showUnSubProc .= ')';
	
	$hideUnSubProc = 'hideUnSub(';
	$hideUnSubProc .= $unSubButtonID;
	$hideUnSubProc .= ')';
	
	require('_detail.php');
?>

<li class="span2 feed-pin">
    <div id="feed-thumbnail" class="thumbnail" onMouseOver="mouseOver(<?php echo $unSubButtonID ?>, <?php echo $modifyButtonID ?>)" onMouseOut="mouseOut(<?php echo $unSubButtonID ?>, <?php echo $modifyButtonID ?>)">
		<div id="feed-thumb-header">
			<h6 class="feed-name"><?php echo CHtml::encode($data->name); ?></h6>
			<?php if(Yii::app()->user->id == $data->user_id) { ?>
			<div class="feed-wrapper-close">
				<?php 
					echo CHtml::ajaxLink(
						"×",
						Yii::app()->createUrl('feed/delete', array('id'=>$data->id)),
						array(	
							'type'=>'POST',
							'dataType'=>'json', 
							'data'=>array(),
							'complete'=>'js:function(jqXHR, textStatus){$.fn.yiiListView.update("feed-thumb");}'
						),
						array( 
							'class' => 'close',
							//'style' => 'display:none',
						)
					);
				?>
				<?php } ?>
			</div>
		</div>
			
		<?php 
			$clickFunc = "showFeedModal(";
			$clickFunc .= $data->id;
			$clickFunc .= ",'";
			$clickFunc .= $data->name;
			$clickFunc .= "','";
			$clickFunc .= $data->link;
			$clickFunc .= "','";
			$clickFunc .= $data->mid_img_link;
			$clickFunc .= "','";
			$clickFunc .= $data->category->name;
			$clickFunc .= "','";
			$clickFunc .= $data->description;
			$clickFunc .= "')";
		?>
		<a href="#" rel="tooltip" onClick="<?php echo $clickFunc ?>" data-title="">
			<img class="thumImg" src="<?php echo CHtml::encode($data->thumb_img_link); ?>" alt="" >
		</a>
		<!--p class="feed-category">分类:<?php echo CHtml::encode($data->category->name); ?></p!-->

		<?php if( !Yii::app()->user->isGuest ){ ?>
		<div class="sub-wrapper-bottom-right">	
			<?php 	
				if (isset($data->subscribe[0]))
				{
					$buttonLabel = '已订阅';
					$type = 'success';
					$beforeSend = 'js:function(){if(confirm("确定要取消该订阅吗?"))return true;else return false;}';
				}
				else
				{
					$buttonLabel = '订阅';
					$type = 'primary';
					$beforeSend = null;
				}
				$this->widget('bootstrap.widgets.TbButton', array(
									'label'=>$buttonLabel,
									'buttonType'=>'Button',
									'type'=>$type, // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
									'size'=>'mini',    // null, 'large', 'small' or 'mini'	
									'htmlOptions'=>array(	
											'id' => $unSubButtonID,
											'style' => 'display:none',
											'onMouseOver' => $showUnSubProc,
											'onMouseOut' => $hideUnSubProc,
											'onclick'=>' {'.CHtml::ajax( array(
															   'beforeSend'=>$beforeSend,
															    'type'=>"POST",
																'url' => Yii::app()->createUrl('subscribe/update', array('feed_id'=>$data->id)),
															    'complete' => 'js:function(jqXHR, textStatus) {
																			$.fn.yiiListView.update("feed-thumb");
																			}')
															).'return false;}',
									),
			)); ?>
			
			<?php if(Yii::app()->user->id == $data->user_id) { ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
									'label'=>'修改',
									'type'=>'warning', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
									'size'=>'mini',    // null, 'large', 'small' or 'mini'
									'url' => Yii::app()->createUrl('feed/update', array('id'=>$data->id)),
									'htmlOptions'=>array(	
											'id' => $modifyButtonID,
											'style' => 'display:none',
									),
			)); ?>
			<?php } ?>
	
		</div>
		<?php } ?>
    </div>
</li>