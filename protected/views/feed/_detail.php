
<script language="javascript">
function showFeedModal(id, name, url, imgUrl, categoryName, description){
	$("#feed-modal-name").html(name);
	$("#feed-modal-url").attr("href",url);
	$("#feed-modal-img-url").attr("src",imgUrl);
	var category = "分类：" + categoryName;
	$("#feed-modal-category").html(category);
	$("#feed-modal-description").html(description);
	$('#feed-modal').modal('show', function (e) {
    
	})
}

</script> 

<?php 
	$this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'feed-modal',)); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4 id="feed-modal-name"></h4>
</div>
 
<div id="feed-detail" class="thumbnail">
		<a id="feed-modal-url" href="" target="_blank" rel="tooltip" data-title="">
			<img id="feed-modal-img-url" src="" alt="" >
		</a>
		<p id="feed-modal-category" class='feed-category'></p>
		<p id="feed-modal-description" class='feed-description'></p>
</div>
 
<?php $this->endWidget(); ?>
