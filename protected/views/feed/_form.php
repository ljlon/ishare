<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'feed-form',
	'enableAjaxValidation'=>false,
)); ?>

	<!--p class="help-block">Fields with <span class="required">*</span> are required.</p --!>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'thumb_img_link',array('class'=>'span5','maxlength'=>100)); ?>
	
	<?php echo $form->textFieldRow($model,'mid_img_link',array('class'=>'span5','maxlength'=>100)); ?>
	
	<?php echo $form->textFieldRow($model,'original_img_link',array('class'=>'span5','maxlength'=>100)); ?>

	<?php 
		$feedCateArray = array();
		foreach ($dataProvider->getData() as $key=>$value)
		{
			$feedCateArray[$value->id] = $value->name;
		}
		echo $form->dropDownListRow($model, 'category_id', $feedCateArray);
	?>
	
	<?php echo $form->textAreaRow($model,'description',array('class'=>'span5','rows'=>8,'maxlength'=>1000)); ?>

	<div>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? '分享' : '保存',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
