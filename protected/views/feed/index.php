<?php
	/* @var $this SiteController */

	$this->pageTitle=Yii::app()->name;

	if (isset($categoryID))
	{
		$this->sortMenu=array(
			array('label'=>'最多订阅','url'=>Yii::app()->createUrl('feed/index', array('by'=>0, 'cat'=>$categoryID))),
			array('label'=>'最近更新','url'=>Yii::app()->createUrl('feed/index', array('by'=>1, 'cat'=>$categoryID))),
			array('label'=>'随便看看','url'=>Yii::app()->createUrl('feed/index', array('by'=>2, 'cat'=>$categoryID))),
		);
	}
	else
	{
		$this->sortMenu=array(
			array('label'=>'最多订阅','url'=>Yii::app()->createUrl('feed/index', array('by'=>0))),
			array('label'=>'最近更新','url'=>Yii::app()->createUrl('feed/index', array('by'=>1))),
			array('label'=>'随便看看','url'=>Yii::app()->createUrl('feed/index', array('by'=>2))),
		);
	}
	if (isset($sortID))
	{
		$this->sortMenu[$sortID]['active']=true;
	}

	$links = null;
	foreach ($categoryProvider->getData() as $key=>$value)
	{
		if (isset($sortID))
		{
			$this->category[$value->id] = array('label'=>$value->name,'url'=>Yii::app()->createUrl('feed/index', array('by'=>$sortID, 'cat'=>$value->id)));
		}
		else
		{
			$this->category[$value->id] = array('label'=>$value->name,'url'=>Yii::app()->createUrl('feed/index', array('cat'=>$value->id)));
		}
	}
	if (isset($categoryID))
	{
		$this->category[$categoryID]['active']=true;
		$links = array($this->category[$categoryID]['label']);
	}

	$linkParam = array();
	if (isset($sortID)){
		$linkParam['by']=$sortID;
	}

	$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
		'homeLink'=>CHtml::link('全部', Yii::app()->createUrl('feed/index', $linkParam)), 
		'links'=>$links,
)); ?>

<?php 	if ($dataProvider->itemCount != 0): 
			$this->widget('bootstrap.widgets.TbThumbnails', array(
				'id'=>'feed-thumb',
				'dataProvider'=>$dataProvider,
				'template'=>"{items}\n{pager}",
				'itemView'=>'/feed/_timeline_view',
			)); 
		endif;
?>

