<?php
$data=$model;
$this->menu=array(
	array('label'=>'我的分享','url'=>array('home'),'icon'=>'eye-open'),
	array('label'=>'分享酷站','url'=>array('create'),'icon'=>'icon-plus'),
	array('label'=>'查看','url'=>'#','icon'=>'eye-open','active'=>true),
);
?>

<div class="modal-header">
    <h4><?php echo CHtml::encode($data->name); ?></h4>
</div>
 
<div id="feed-detail" class="thumbnail">
		<a href="<?php echo CHtml::encode($data->link); ?>" rel="tooltip" data-title="">
			<img src="<?php echo CHtml::encode($data->mid_img_link); ?>" alt="" >
		</a>

		<p><?php echo CHtml::encode($data->description); ?></p>
		
		<?php if( !Yii::app()->user->isGuest ){ ?>
		<div>
			<?php 	
				if (isset($data->subscribe[0]))
				{
					$buttonLabel = '已订阅';
					$postData = array();
					$url = Yii::app()->createUrl('subscribe/delete', array('id'=>$data->subscribe[0]['id']));
				}
				else
				{
					$buttonLabel = '订阅';
					$postData = array('subscribe'=>array(
													'feed_id'=>$data->id,
													'user_id'=>Yii::app()->user->id,
													)
								);
					$url = Yii::app()->createUrl('subscribe/create');
				}
				$this->widget('bootstrap.widgets.TbButton', array(
									'label'=>$buttonLabel,
									'buttonType'=>'ajaxButton',
									'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
									'size'=>'mini',    // null, 'large', 'small' or 'mini'
									'url' => $url,		
									'ajaxOptions' => array(
										'type'=>"POST",
										'dataType'=>'json', 
										'data'=>$postData,	
										'complete' => 'js:function(jqXHR, textStatus) {
															$.fn.yiiListView.update("feed-thumb");}',
									),
			)); ?>
			
			<?php if(Yii::app()->user->id == $data->user_id) { ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
									'label'=>'删除',
									'buttonType'=>'ajaxButton',
									'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
									'size'=>'mini',    // null, 'large', 'small' or 'mini'
									'url' => Yii::app()->createUrl('feed/delete', array('id'=>$data->id)),
									'ajaxOptions' => array(
										'type'=>"POST",
										'dataType'=>'json', 					
										'update'=>'#feed-detail'
									),
			)); ?>
			<?php } ?>
	</div>
	<?php } ?>
</div>
