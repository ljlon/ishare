<?php
$this->pageTitle=Yii::app()->name . ' - 登陆';
?>

<div id="register" >
	<h4 class="register-head">欢迎加入</h4>
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'type'=>'horizontal',
		'id'=>'register-form',
		'enableAjaxValidation'=>true,
	)); ?>

		<?php echo $form->textFieldRow($model,'username', array('class'=>'span2')); ?>

		<?php echo $form->textFieldRow($model,'email', array('class'=>'span2')); ?>

		<?php echo $form->passwordFieldRow($model,'password', array('class'=>'span2')); ?>

		<?php echo $form->passwordFieldRow($model,'passwordAgain', array('class'=>'span2')); ?>

		<?php //echo $form->passwordFieldRow($model,'password2',array('class'=>'span5','maxlength'=>100)); ?>

		<div class="control-group">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'注册',
				'htmlOptions'=>array(
					'class'=>'span2 controls',
					),
			)); ?>
		</div>

	<?php $this->endWidget(); ?>
</div>
