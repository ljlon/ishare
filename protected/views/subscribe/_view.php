<?php
	$subOrder = $data;
	$subscribe = $subOrder->subscribe;
	$data = $subscribe->feed;
	
	$dragID = $subscribe->id;
	$dropID = 'drop';
	$dropID .= $subscribe->id;
	
	$unSubButtonID = 'unSubButton';
	$unSubButtonID .= $subscribe->id;
	$feedButtonID = 'feedButton';
	$feedButtonID .= $subscribe->id;
		
	$showUnSubProc = 'showUnSub(';
	$showUnSubProc .= $unSubButtonID;
	$showUnSubProc .= ')';
	
	$hideUnSubProc = 'hideUnSub(';
	$hideUnSubProc .= $unSubButtonID;
	$hideUnSubProc .= ')';
				
	require_once(dirname(__FILE__).'/../feed/_detail.php');
?>

<li class="span2 sub-pin">
	<?php $this->beginWidget('zii.widgets.jui.CJuiDroppable', array(  
							'options'=>array(  
											'activeClass'=>'ui-state-hover',
											'hoverClass'=>'ui-state-active',
											/*'over'=>'js:function(event,ui){
												$(this).html("Activated!")
											}', */
											'drop'=>'js:function(event,ui){
												if (dragParent[0] != $(this)[0])
												{
													var srcSubID = dragParent.children(":first");
													var dstSubID = $(this).children(":first");
													
													dragParent.append(
														$(".dragItem", this).remove().clone()
														.removeClass().addClass("dragItem")
														.css({"left": "", "opacity": "","top":""})
														.draggable(dragOptions)
													);
													
													$.ajax({  
														type: "POST", 
														dataType:"json",														
														url : ajaxSwapUrl,  
														data: {"srcSubID":srcSubID.attr("id"),"dstSubID":dstSubID.attr("id")},        
													}); 						
												}
												
												$(this).append(
													ui.draggable.remove().clone()
													.removeClass().addClass("dragItem")
													.css({"left": "", "opacity": "","top":""})
													.draggable(dragOptions)
												);
												
												bDragging = false;
											}', 
										),  
							'htmlOptions'=>array(
										'id'=>$dropID,
										),
									));  
	?>  
	<?php $this->beginWidget('zii.widgets.jui.CJuiDraggable', array(
							'options'=>array(
										'cursor'=>'move',
										'revert'=>'invalid',
										'opacity'=>0.8,
										'start'=>'js:function( event, ui ){
											dragParent = $(this).parent();
											bDragging = true;
										}',
										'stop'=>'js:function( event, ui ){
											bDragging = false;
										}',
									),
							'htmlOptions'=>array(
										'id'=>$dragID,
										'class'=>'dragItem',
									),
								)); 
	?>
    <div id="sub-thumbnail" class="thumbnail" onMouseOver="mouseOver(<?php echo $unSubButtonID ?>, <?php echo isset($subscribe->feed)?$feedButtonID:'null'; ?>)" onMouseOut="mouseOut(<?php echo $unSubButtonID ?>, <?php echo isset($subscribe->feed)?$feedButtonID:'null'; ?>)" >
		<div id="sub-thumb-header">
			<?php if (isset($subscribe->feed)) : ?>
				<h6 class="sub-name"><?php echo CHtml::encode($subscribe->feed->name); ?></h6>
			<?php endif; ?>
			<div class="sub-wrapper-close">
				<?php 
					echo CHtml::Link(
						"×",
						Yii::app()->createUrl('subscribe/delete', array('id'=>$subscribe->id)),
						array( 
							'id' => $unSubButtonID,
							'class' => 'close',
							'style' => 'display:none',
							'onclick'=>' {'.CHtml::ajax( array(
															   'beforeSend'=>'js:function(){if(confirm("确定要取消该订阅吗?"))return true;else return false;}',
															    'type'=>"POST",
																'url' => Yii::app()->createUrl('subscribe/delete', array('id'=>$subscribe->id)),
															    'complete' => 'js:function(jqXHR, textStatus) {
																			$.fn.yiiListView.update("sub-thumb");
																			}')
															).'return false;}',
						)
					);
				?>
			</div>
		</div>
		<div class="sub-wrapper-bottom-right">	
			<?php 
			if (isset($subscribe->feed)) : 	
				$clickFunc = "showFeedModal(";
				$clickFunc .= $subscribe->feed->id;
				$clickFunc .= ",'";
				$clickFunc .= $subscribe->feed->name;
				$clickFunc .= "','";
				$clickFunc .= $subscribe->feed->link;
				$clickFunc .= "','";
				$clickFunc .= $subscribe->feed->mid_img_link;
				$clickFunc .= "','";
				$clickFunc .= $subscribe->feed->category->name;
				$clickFunc .= "','";
				$clickFunc .= $data->description;
				$clickFunc .= "')";
				$this->widget('bootstrap.widgets.TbButton', array(
									'label'=>'查看',
									'type'=>'info',
									'size'=>'mini',
									'htmlOptions'=>array(
										'id'=>$feedButtonID,
										'style' => 'display:none;',
										'onclick'=>"$clickFunc"
										//'data-toggle'=>'modal',
										//'data-target'=>'#madol',
									),
				)); 
			else:
				$this->widget('bootstrap.widgets.TbButton', array(
									'label'=>'',
									'type'=>'info',
									'size'=>'mini',
									'htmlOptions'=>array(
										'style' => 'display:none;',
									),
				)); 
			endif; ?>
		</div>
		
		<div>
		<?php if (isset($subscribe->feed)) : ?>
			<a href="<?php echo CHtml::encode($subscribe->feed->link); ?>" target="_blank" rel="tooltip" data-title="">
				<img class="thumImg" src="<?php echo CHtml::encode($subscribe->feed->thumb_img_link); ?>" alt="" >
			</a>
		<?php else : ?>
			<p><?php echo "订阅源已被删除";?></p>
		<?php endif; ?>
		</div>
		
	 </div>
	 
<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
</li >

